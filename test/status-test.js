#!/usr/bin/gjs
/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

imports.gi.versions.Reactor = '0.1';
imports.gi.versions.GLib = '2.0';

const Reactor = imports.gi.Reactor;
const GLib = imports.gi.GLib;

let tty = new Reactor.ComTty ({ "device": "/dev/ttyUSB0" });
let loop = new GLib.MainLoop (null, false);

tty.connect ("receive-status", (tty, flags) => {
    print ("received status " + flags);
    loop.quit();
});

GLib.timeout_add (GLib.PRIORITY_DEFAULT, 1000, () => {
    print ("timeout");

    loop.quit ();
});

tty.send_set (Reactor.ComStatus.CHARGING);
tty.send_query (Reactor.ComQuery.STATUS);

loop.run ();
