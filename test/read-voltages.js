#!/usr/bin/gjs
/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

imports.gi.versions.Reactor = '0.1';
imports.gi.versions.GLib = '2.0';

const Reactor = imports.gi.Reactor;
const GLib = imports.gi.GLib;

let tty = new Reactor.ComTty ({ "device": "/dev/ttyUSB0" });
let loop = new GLib.MainLoop (null, false);

tty.connect ("receive-voltage", (tty, values) => {
    print ("received " + values.length + " voltages:");

    for (var voltage of values) {
	print (voltage);
    }

    loop.quit();
});

GLib.timeout_add (GLib.PRIORITY_DEFAULT, 1000, () => {
    print ("timeout");

    loop.quit ();
});

tty.send_query (Reactor.ComQuery.VOLTAGE);

loop.run ();
