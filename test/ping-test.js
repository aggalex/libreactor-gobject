#!/usr/bin/gjs

/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

imports.gi.versions.Reactor = '0.1';
imports.gi.versions.GLib = '2.0';

const Reactor = imports.gi.Reactor;
const GLib = imports.gi.GLib;

let tty = new Reactor.ComTty ({ "device": "/dev/ttyUSB0" });
let loop = new GLib.MainLoop (null, false);

tty.connect ("receive-message", (tty, msg) => {
    print ("received message: " + msg);
});

tty.connect ("receive-ping", (tty, ack) => {
    print ("received ping: " + ack);

    if (ack > 0) {
	tty.send_ping (ack - 1);
    }

    if (ack < 2) {
	loop.quit ();
    }
});

tty.send_timestamp (1234);
tty.send_message ("hello, reactor!");
tty.send_ping (255);

loop.run ();
