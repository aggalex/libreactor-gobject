/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

/**
 * SECTION:reactor-com-serial
 * @short_description: serial implementation of reactor com
 * @title:ReactorComSerial
 *
 * #ReactorComSerial implements #ReactorCom that sends commands over
 * an abstract byte stream. The class serializes and deserializes commands
 * that can be sent over over provided transfer medium.
 */

/**
 * ReactorComSerialClass:
 * @write_data: virtual write method
 */

#include "reactor-com-serial.h"

#include <rcom-serial.h>

struct com_serial
{
    struct rcom_serial base;
    ReactorComSerial *self;
};

typedef struct {
    struct com_serial serial;
} ReactorComSerialPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (ReactorComSerial, reactor_com_serial, REACTOR_TYPE_COM);

enum
{
    SIGNAL_WRITE,
    N_SIGNALS,
};

static guint signals[N_SIGNALS];
static void constructed (GObject *gobj);
static void finalize (GObject *gobj);
static gint write_data (ReactorComSerial *self,
			gconstpointer data,
			gint bytes,
			GError **error);
static int16_t com_write (struct rcom_serial *serial,
			  const void *data,
			  uint16_t bytes);

static void
reactor_com_serial_init (ReactorComSerial *self G_GNUC_UNUSED)
{
}

static void
reactor_com_serial_class_init (ReactorComSerialClass *cls)
{
    GObjectClass *gcls = G_OBJECT_CLASS (cls);

    cls->write_data = write_data;
    gcls->constructed = constructed;
    gcls->finalize = finalize;

    /**
     * ReactorComSerial::write:
     * @self:
     * @data: (array length=bytes) (element-type guint8):
     * @bytes:
     */
    signals[SIGNAL_WRITE] =
	g_signal_new ("write",
		      REACTOR_TYPE_COM_SERIAL,
		      G_SIGNAL_RUN_LAST,
		      0, NULL, NULL, NULL,
		      G_TYPE_INT, 2, G_TYPE_POINTER, G_TYPE_INT);
}

static void
constructed (GObject *gobj)
{
    ReactorComSerial *self;
    ReactorComSerialPrivate *p;
    struct rcom *receiver;

    G_OBJECT_CLASS (reactor_com_serial_parent_class)->constructed (gobj);

    self = REACTOR_COM_SERIAL (gobj);
    p = reactor_com_serial_get_instance_private (self);

    g_object_get (self, "receiver", &receiver, NULL);

    rcom_serial_init ((struct rcom_serial *) &p->serial,
		      com_write, receiver);
    p->serial.self = self;

    g_object_set (self, "transmitter", &p->serial, NULL);
}

static void
finalize (GObject *gobj)
{
    G_OBJECT_CLASS (reactor_com_serial_parent_class)->finalize (gobj);
}

static gint
write_data (ReactorComSerial *self,
	    gconstpointer data,
	    gint bytes,
	    GError **error)
{
    gint written;

    written = 0;

    g_signal_emit (self, signals[SIGNAL_WRITE], 0,
		   data, bytes, &written);

    if (written != bytes) {
	g_set_error (error,
		     REACTOR_COM_ERROR,
		     REACTOR_COM_ERROR_SEND,
		     "written %d of %d bytes",
		     written, bytes);
	return -1;
    }

    return written;
}

static int16_t
com_write (struct rcom_serial *serial,
	   const void *data,
	   uint16_t bytes)
{
    g_autoptr (GError) error = NULL;
    ReactorComSerial *self;
    gint ret;

    self = ((struct com_serial *) serial)->self;
    ret = reactor_com_serial_write_data (self, data, bytes, &error);

    if (ret < 0) {
	g_warning ("%s", error->message);
    }

    return ret;
}

ReactorCom *
reactor_com_serial_new ()
{
    return g_object_new (REACTOR_TYPE_COM_SERIAL, NULL);
}

/**
 * reactor_com_serial_write_data: (virtual write_data)
 * @self: self
 * @data: (array length=bytes) (element-type guint8): Raw byte array to write
 * @bytes: Number of bytes to write
 * @error: GError location
 */
gint
reactor_com_serial_write_data (ReactorComSerial *self,
			       gconstpointer data,
			       gint bytes,
			       GError **error)
{
    return REACTOR_COM_SERIAL_GET_CLASS (self)->write_data (self, data,
							    bytes, error);
}

/**
 * reactor_com_serial_feed_data:
 * @self: self
 * @data: (array length=bytes) (element-type guint8): Received data to parse
 * @bytes: Number of received bytes to parse
 */
gint
reactor_com_serial_feed_data (ReactorComSerial *self,
			      gconstpointer data,
			      gint bytes)
{
    ReactorComSerialPrivate *p;

    p = reactor_com_serial_get_instance_private (self);

    return rcom_serial_feed ((struct rcom_serial *) &p->serial, data, bytes);
}
