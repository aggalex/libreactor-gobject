/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

#include "reactor-com-serial.h"

G_BEGIN_DECLS

#define REACTOR_TYPE_COM_TTY (reactor_com_tty_get_type ())

G_DECLARE_DERIVABLE_TYPE (ReactorComTty, reactor_com_tty,
			  REACTOR, COM_TTY, ReactorComSerial);

struct _ReactorComTtyClass
{
    ReactorComSerialClass parent_class;
};

ReactorCom *reactor_com_tty_new (const gchar *device);
gint reactor_com_tty_get_file_desc (ReactorComTty *self,
				    GError **error);
void reactor_com_tty_set_device (ReactorComTty *self,
				 const gchar *device);
const gchar *reactor_com_tty_get_device (ReactorComTty *self);

G_END_DECLS
