/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

/**
 * SECTION:reactor-com-tty
 * @short_description: unix tty implementation of #ReactorComSerial
 * @title:ReactorComTty
 *
 * #ReactorComTty implements #ReactorComSerial to use serial device given with
 * given path, i.e. /dev/ttyUSB0.
 */

#include "reactor-com-tty.h"

#include <unistd.h>
#include <fcntl.h>
#include <asm/termios.h>

extern int ioctl (int, unsigned long, ...);

typedef struct {
    gchar *device;
    gint file_desc;
    GIOChannel *io_channel;
    guint io_watch;
} ReactorComTtyPrivate;

enum
{
    PROP_0,
    PROP_DEVICE,
    N_PROPS,
};

G_DEFINE_TYPE_WITH_PRIVATE (ReactorComTty, reactor_com_tty,
			    REACTOR_TYPE_COM_SERIAL);

static GParamSpec *props[N_PROPS];

static void set_property (GObject *gobj, guint propid,
			  const GValue *value, GParamSpec *spec);
static void get_property (GObject *gobj, guint propid,
			  GValue *value, GParamSpec *spec);
static void finalize (GObject *gobj);
static gint write_data (ReactorComSerial *serial,
			gconstpointer data,
			gint bytes,
			GError **error);
static void close_file_desc (ReactorComTty *self);

static void
reactor_com_tty_init (ReactorComTty *self)
{
    ReactorComTtyPrivate *p;

    p = reactor_com_tty_get_instance_private (self);

    p->file_desc = -1;
}

static void
reactor_com_tty_class_init (ReactorComTtyClass *cls)
{
    GObjectClass *gcls = G_OBJECT_CLASS (cls);
    ReactorComSerialClass *scls = REACTOR_COM_SERIAL_CLASS (cls);

    gcls->set_property = set_property;
    gcls->get_property = get_property;
    gcls->finalize = finalize;
    scls->write_data = write_data;

    props[PROP_DEVICE] =
	g_param_spec_string ("device",
			     "device",
			     "device path",
			     NULL,
			     G_PARAM_READWRITE |
			     G_PARAM_CONSTRUCT |
			     G_PARAM_STATIC_STRINGS);

    g_object_class_install_properties (gcls, N_PROPS, props);
}

static void
set_property (GObject *gobj, guint propid,
	      const GValue *value, GParamSpec *spec)
{
    ReactorComTty *self;

    self = REACTOR_COM_TTY (gobj);

    switch (propid) {
    case PROP_DEVICE:
	reactor_com_tty_set_device (self, g_value_get_string (value));
	break;

    default:
	G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, propid, spec);
    }
}

static void
get_property (GObject *gobj, guint propid,
	      GValue *value, GParamSpec *spec)
{
    ReactorComTty *self;
    ReactorComTtyPrivate *p;

    self = REACTOR_COM_TTY (gobj);
    p = reactor_com_tty_get_instance_private (self);

    switch (propid) {
    case PROP_DEVICE:
	g_value_set_string (value, p->device);
	break;

    default:
	G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, propid, spec);
    }
}

static void
finalize (GObject *gobj)
{
    close_file_desc (REACTOR_COM_TTY (gobj));

    G_OBJECT_CLASS (reactor_com_tty_parent_class)->finalize (gobj);
}

static gint
write_data (ReactorComSerial *serial,
	    gconstpointer data,
	    gint bytes,
	    GError **error)
{
    ReactorComTty *self;
    gint file_desc, written;

    self = REACTOR_COM_TTY (serial);
    file_desc = reactor_com_tty_get_file_desc (self, error);

    if (file_desc < 0) {
	return -1;
    }

    written = write (file_desc, data, bytes);

    if (written < 0) {
	g_set_error (error,
		     REACTOR_COM_ERROR,
		     REACTOR_COM_ERROR_SEND,
		     "cannot write to the device: %s",
		     reactor_com_tty_get_device (self));
	close_file_desc (self);
	return -1;
    }

    if (written != bytes) {
	g_set_error (error,
		     REACTOR_COM_ERROR,
		     REACTOR_COM_ERROR_SEND,
		     "written only %d of %d bytes",
		     written, bytes);
	return -1;
    }

    return written;
}

static void
close_file_desc (ReactorComTty *self)
{
    ReactorComTtyPrivate *p;

    p = reactor_com_tty_get_instance_private (self);

    if (p->io_watch > 0) {
	g_source_remove (p->io_watch);
	p->io_watch = 0;
    }

    if (p->io_channel != NULL) {
	g_io_channel_shutdown (p->io_channel, FALSE, NULL);
	g_clear_pointer (&p->io_channel, g_io_channel_unref);
    }

    if (p->file_desc >= 0) {
	close (p->file_desc);
	p->file_desc = -1;
    }
}

static gboolean
on_read_watch (GIOChannel *channel G_GNUC_UNUSED,
	       GIOCondition cond G_GNUC_UNUSED,
	       gpointer user_data)
{
    ReactorComTtyPrivate *p;
    ReactorComTty *self;
    gchar buffer[256];
    gint readno;

    self = user_data;
    p = reactor_com_tty_get_instance_private (self);
    readno = read (p->file_desc, buffer, sizeof (buffer));

    if (readno > 0) {
	reactor_com_serial_feed_data (REACTOR_COM_SERIAL (self),
				      buffer, readno);
    }

    return TRUE;
}

/**
 * reactor_com_tty_new:
 * @device: (nullable): device path
 *
 * Creates #ReactorComTty for given device path.
 *
 * returns: (transfer full): new #ReactorComTty
 */
ReactorCom *
reactor_com_tty_new (const gchar *device)
{
    return g_object_new (REACTOR_TYPE_COM_TTY,
			 "device", device,
			 NULL);
}

/**
 * reactor_com_tty_get_file_desc:
 * @self: a #ReactorComTty
 * @error: (out) (nullable): location of #GError
 *
 * Opens the file and gets the descriptor.
 *
 * returns: file descriptor, >=0 if valid or <0 otherwise
 */
gint
reactor_com_tty_get_file_desc (ReactorComTty *self,
			       GError **error)
{
    ReactorComTtyPrivate *p;
    struct termios2 conf;

    p = reactor_com_tty_get_instance_private (self);

    if (p->file_desc < 0) {
	close_file_desc (self);

	p->file_desc = open (p->device, O_RDWR | O_NOCTTY | O_NDELAY);

	if (p->file_desc < 0) {
	    g_set_error (error,
			 REACTOR_COM_ERROR,
			 REACTOR_COM_ERROR_OPEN,
			 "cannot open file: %s",
			 p->device);
	    return -1;
	}

	if (isatty (p->file_desc) == 0) {
	    g_set_error (error,
			 REACTOR_COM_ERROR,
			 REACTOR_COM_ERROR_OPEN,
			 "file %s is not a tty device",
			 p->device);
	    close_file_desc (self);
	    return -1;
	}

	memset (&conf, 0, sizeof (conf));

	conf.c_ispeed = 115200;
	conf.c_ospeed = 115200;
	conf.c_cflag |= BOTHER | CS8 | CREAD | CLOCAL;
	conf.c_cc[VMIN] = 0;
	conf.c_cc[VTIME] = 10;

	if (ioctl (p->file_desc, TCSETS2, &conf) < 0) {
	    g_set_error (error,
			 REACTOR_COM_ERROR,
			 REACTOR_COM_ERROR_OPEN,
			 "cannot configure tty device: %s",
			 p->device);
	    close_file_desc (self);
	    return -1;
	}

	p->io_channel = g_io_channel_unix_new (p->file_desc);
	p->io_watch = g_io_add_watch (p->io_channel,
				      G_IO_IN,
				      on_read_watch,
				      self);
    }

    return p->file_desc;
}

/**
 * reactor_com_tty_set_device:
 * @self: a #ReactorComTty
 * @device: new device path
 *
 * Sets device path.
 */
void
reactor_com_tty_set_device (ReactorComTty *self,
			    const gchar *device)
{
    ReactorComTtyPrivate *p;

    p = reactor_com_tty_get_instance_private (self);

    if (g_strcmp0 (device, p->device) != 0) {
	g_clear_pointer (&p->device, g_free);
	p->device = g_strdup (device);
	close_file_desc (self);
	g_object_notify (G_OBJECT (self), "device");
    }
}

/**
 * reactor_com_tty_get_device:
 * @self: a #ReactorComTty
 *
 * Gets device path.
 *
 * returns: (transfer none): device path
 */
const gchar *
reactor_com_tty_get_device (ReactorComTty *self)
{
    ReactorComTtyPrivate *p;

    p = reactor_com_tty_get_instance_private (self);

    return p->device;
}
