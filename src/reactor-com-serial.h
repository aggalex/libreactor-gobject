/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

#include "reactor-com.h"

G_BEGIN_DECLS

#define REACTOR_TYPE_COM_SERIAL (reactor_com_serial_get_type ())

G_DECLARE_DERIVABLE_TYPE (ReactorComSerial, reactor_com_serial,
			  REACTOR, COM_SERIAL, ReactorCom)

struct _ReactorComSerialClass
{
    ReactorComClass parent_class;

    gint    (*write_data)    (ReactorComSerial *self,
			     gconstpointer data,
			     gint bytes,
			     GError **error);
};

ReactorCom *reactor_com_serial_new ();
gint reactor_com_serial_write_data (ReactorComSerial *self,
				    gconstpointer data,
				    gint bytes,
				    GError **error);
gint reactor_com_serial_feed_data (ReactorComSerial *self,
				   gconstpointer data,
				   gint bytes);

G_END_DECLS
