/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

#include "reactor-com.h"

G_BEGIN_DECLS

#define REACTOR_TYPE_COM_DUMMY (reactor_com_dummy_get_type ())

G_DECLARE_DERIVABLE_TYPE (ReactorComDummy, reactor_com_dummy,
			  REACTOR, COM_DUMMY, ReactorCom);

struct _ReactorComDummyClass
{
    ReactorComClass parent_class;
};

ReactorCom *reactor_com_dummy_new (const gchar *config_path);

G_END_DECLS
