#include "reactor-types.h"

/**
 * ReactorComStatus:
 * @REACTOR_COM_STATUS_CHARGING: charging
 * @REACTOR_COM_STATUS_DISCHARGING: discharging
 * @REACTOR_COM_STATUS_PROTECTED: protected
 * @REACTOR_COM_STATUS_LOCKED: locked
 *
 * Status flags. Represent a state of the device.
 */

/**
 * ReactorComProtection:
 * @REACTOR_COM_PROTECTION_CELL_UV: cell-uv
 * @REACTOR_COM_PROTECTION_CELL_OV: cell-ov
 * @REACTOR_COM_PROTECTION_CHARGING_OT: charging-ot
 * @REACTOR_COM_PROTECTION_CHARGING_UT: charging-ot
 * @REACTOR_COM_PROTECTION_DISCHARGING_OT: discharging-ot
 * @REACTOR_COM_PROTECTION_DISCHARGING_UT: discharging-ut
 * @REACTOR_COM_PROTECTION_CHARGING_OC: charging-oc
 * @REACTOR_COM_PROTECTION_DISCHARGING_OC: discharging-oc
 *
 * Protection flags that represent different battery protection sources.
 */

/**
 * ReactorComQuery:
 * @REACTOR_COM_QUERY_TIMESTAMP: timestamp
 * @REACTOR_COM_QUERY_VOLTAGE: voltage
 * @REACTOR_COM_QUERY_CURRENT: current
 * @REACTOR_COM_QUERY_STATUS: status
 * @REACTOR_COM_QUERY_PROTECTION: protection
 * @REACTOR_COM_QUERY_BALANCING: balancing
 * @REACTOR_COM_QUERY_TEMPERATURE: temperature
 * @REACTOR_COM_QUERY_CONFIG: config
 * @REACTOR_COM_QUERY_CAPLIMIT: caplimit
 *
 * Query flags that are used as an argument to query command. Many flags can be
 * used toghether using bit-or operator.
 */

/**
 * ReactorComBq769x0:
 * @REACTOR_COM_BQ769X0_WRITE: write
 * @REACTOR_COM_BQ769X0_READ: read
 * @REACTOR_COM_BQ769X0_DATA: data
 */
