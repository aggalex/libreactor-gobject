/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

/**
 * SECTION:reactor-com
 * @short_description: abstract communication node
 * @title: ReactorCom
 *
 * The #ReactorCom class represents an abstract bidirectional communication
 * node for the reactor communication. It provides common methods and signals
 * that wraps low-level rcom structure.
 */

#include "reactor-com.h"
#include "reactor-config.h"

#include <rcom.h>

struct com_receiver
{
    struct rcom base;
    ReactorCom *self;
};

static int8_t api_ping (struct rcom *com,
			uint8_t ack);
static int8_t api_timestamp (struct rcom *com,
			     int64_t timestamp);
static int8_t api_message (struct rcom *com,
			   const char *message);
static int8_t api_voltage (struct rcom *com,
			   const int16_t *cells,
			   uint8_t count);
static int8_t api_current (struct rcom *com,
			   int32_t current);
static int8_t api_status (struct rcom *com,
			  uint32_t status);
static int8_t api_protection (struct rcom *com,
			      uint32_t protection);
static int8_t api_balancing (struct rcom *com,
			     uint32_t balancing);
static int8_t api_temperature (struct rcom *com,
			       const int16_t *temperatures,
			       uint8_t count);
static int8_t api_config (struct rcom *com,
			  const struct rcom_conf *config);
static int8_t api_caplimit (struct rcom *com,
			    uint8_t limit);
static int8_t api_query (struct rcom *com,
			 uint32_t query);
static int8_t api_set (struct rcom *com,
		       uint32_t status);
static int8_t api_reset (struct rcom *com,
			 uint32_t status);
static int8_t api_bq769x0 (struct rcom *com,
			   uint8_t action,
			   uint8_t address,
			   uint8_t size,
			   const uint8_t *data);

struct rcom_api api_class = {
    .ping = api_ping,
    .timestamp = api_timestamp,
    .message = api_message,
    .voltage = api_voltage,
    .current = api_current,
    .status = api_status,
    .protection = api_protection,
    .balancing = api_balancing,
    .temperature = api_temperature,
    .config = api_config,
    .caplimit = api_caplimit,
    .query = api_query,
    .set = api_set,
    .reset = api_reset,
    .bq769x0 = api_bq769x0,
};

G_DEFINE_QUARK (reactor-com-error-quark, reactor_com_error);

typedef struct {
    struct com_receiver receiver;
    struct rcom *transmitter;
} ReactorComPrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (ReactorCom, reactor_com, G_TYPE_OBJECT)

enum
{
    PROP_0,
    PROP_RECEIVER,
    PROP_TRANSMITTER,
    N_PROPS,
};

enum
{
    SIGNAL_RECEIVE_PING,
    SIGNAL_RECEIVE_TIMESTAMP,
    SIGNAL_RECEIVE_MESSAGE,
    SIGNAL_RECEIVE_VOLTAGE,
    SIGNAL_RECEIVE_CURRENT,
    SIGNAL_RECEIVE_STATUS,
    SIGNAL_RECEIVE_PROTECTION,
    SIGNAL_RECEIVE_BALANCING,
    SIGNAL_RECEIVE_TEMPERATURE,
    SIGNAL_RECEIVE_CONFIG,
    SIGNAL_RECEIVE_CAPLIMIT,
    SIGNAL_RECEIVE_QUERY,
    SIGNAL_RECEIVE_SET,
    SIGNAL_RECEIVE_RESET,
    SIGNAL_RECEIVE_BQ769X0,
    N_SIGNALS,
};

static gint signals[N_SIGNALS];
static GParamSpec *props[N_PROPS];
static void finalize (GObject *gobj);
static void set_property (GObject *gobj, guint prop_id,
			  const GValue *value, GParamSpec *spec);
static void get_property (GObject *gobj, guint prop_id,
			  GValue *value, GParamSpec *spec);
static struct rcom *get_transmitter (ReactorCom *self);

static void
reactor_com_init (ReactorCom *self)
{
    ReactorComPrivate *p;

    p = reactor_com_get_instance_private (self);

    rcom_init ((struct rcom *) &p->receiver, &api_class, NULL);
    p->receiver.self = self;
}

static void
reactor_com_class_init (ReactorComClass *cls)
{
    GObjectClass *gcls = G_OBJECT_CLASS (cls);

    gcls->finalize = finalize;
    gcls->set_property = set_property;
    gcls->get_property = get_property;

    /**
     * ReactorCom::receive-ping:
     * @self: a #ReactorCom
     * @ack: acknowledge argument, if non zero, ping back with 
     *	 decremented by one
     *
     * Receive PING command.
     */
    signals[SIGNAL_RECEIVE_PING] =
	g_signal_new ("receive-ping",
		      REACTOR_TYPE_COM,
		      G_SIGNAL_RUN_LAST,
		      0, NULL, NULL, NULL,
		      G_TYPE_NONE, 1, G_TYPE_INT);

    /**
     * ReactorCom::receive-timestamp:
     * @self: a #ReactorCom
     * @timestamp: Received current time as number of seconds since 1st Jan
     * 1970 (UNIX timestamp)
     *
     * Receive TIMESTAMP command.
     */
    signals[SIGNAL_RECEIVE_TIMESTAMP] =
	g_signal_new ("receive-timestamp",
		      REACTOR_TYPE_COM,
		      G_SIGNAL_RUN_LAST,
		      0, NULL, NULL, NULL,
		      G_TYPE_NONE, 1, G_TYPE_INT64);

    /**
     * ReactorCom::receive-message:
     * @self: a #ReactorCom
     * @message: Received text message
     *
     * Receive MESSAGE command.
     */
    signals[SIGNAL_RECEIVE_MESSAGE] =
	g_signal_new ("receive-message",
		      REACTOR_TYPE_COM,
		      G_SIGNAL_RUN_LAST,
		      0, NULL, NULL, NULL,
		      G_TYPE_NONE, 1, G_TYPE_STRING);

    /**
     * ReactorCom::receive-voltage:
     * @self: a #ReactorCom
     * @cells: (array length=count) (element-type gint16): cells' voltages
     * @count: number of values
     *
     * Receive VOLTAGE command.
     */
    signals[SIGNAL_RECEIVE_VOLTAGE] =
	g_signal_new ("receive-voltage",
		      REACTOR_TYPE_COM,
		      G_SIGNAL_RUN_LAST,
		      0, NULL, NULL, NULL,
		      G_TYPE_NONE, 2, G_TYPE_POINTER, G_TYPE_INT);

    /**
     * ReactorCom::receive-current:
     * @self: a #ReactorCom
     * @current: Received battery current in miliampers. Positive value for
     * charging current and negative value for discharging current.
     *
     * Receive CURRENT command.
     */
    signals[SIGNAL_RECEIVE_CURRENT] =
	g_signal_new ("receive-current",
		      REACTOR_TYPE_COM,
		      G_SIGNAL_RUN_LAST,
		      0, NULL, NULL, NULL,
		      G_TYPE_NONE, 1, G_TYPE_INT);

    /**
     * ReactorCom::receive-status:
     * @self: a #ReactorCom
     * @status: Received status. Bit values are defined as RCOM_STATUS_*
     * macros.
     *
     * Receive STATUS command.
     */
    signals[SIGNAL_RECEIVE_STATUS] =
	g_signal_new ("receive-status",
		      REACTOR_TYPE_COM,
		      G_SIGNAL_RUN_LAST,
		      0, NULL, NULL, NULL,
		      G_TYPE_NONE, 1, REACTOR_TYPE_COM_STATUS);

    /**
     * ReactorCom::receive-protection:
     * @self: a #ReactorCom
     * @protection: Received protection flags. Bit values are defined as
     * RCOM_PROTECTION_* macros.
     *
     * Receive PROTECTION command.
     */
    signals[SIGNAL_RECEIVE_PROTECTION] =
	g_signal_new ("receive-protection",
		      REACTOR_TYPE_COM,
		      G_SIGNAL_RUN_LAST,
		      0, NULL, NULL, NULL,
		      G_TYPE_NONE, 1, REACTOR_TYPE_COM_PROTECTION);

    /**
     * ReactorCom::receive-balancing:
     * @self: a #ReactorCom
     * @balancing: Balancing flags. Each bit represent once cell.
     *
     * Receive BALANCING command.
     */
    signals[SIGNAL_RECEIVE_BALANCING] =
	g_signal_new ("receive-balancing",
		      REACTOR_TYPE_COM,
		      G_SIGNAL_RUN_LAST,
		      0, NULL, NULL, NULL,
		      G_TYPE_NONE, 1, G_TYPE_UINT);

    /**
     * ReactorCom::receive-temperature:
     * @self: a #ReactorCom
     * @temperatures: (array length=count) (element-type gint16):
     * @count: number of sensors
     *
     * Receive TEMPERATURE command.
     */
    signals[SIGNAL_RECEIVE_TEMPERATURE] =
	g_signal_new ("receive-temperature",
		      REACTOR_TYPE_COM,
		      G_SIGNAL_RUN_LAST,
		      0, NULL, NULL, NULL,
		      G_TYPE_NONE, 2, G_TYPE_POINTER, G_TYPE_INT);

    /**
     * ReactorCom::receive-config:
     * @self: a #ReactorCom
     * @config: ReactorConfig instance
     *
     * Receive CONFIG command.
     */
    signals[SIGNAL_RECEIVE_CONFIG] =
	g_signal_new ("receive-config",
		      REACTOR_TYPE_COM,
		      G_SIGNAL_RUN_LAST,
		      0, NULL, NULL, NULL,
		      G_TYPE_NONE, 1, REACTOR_TYPE_CONFIG);

    /**
     * ReactorCom::receive-caplimit:
     * @self: a #ReactorCom
     * @limit: percentage value of the limit
     *
     * Receive CAPLIMIT command.
     */
    signals[SIGNAL_RECEIVE_CAPLIMIT] =
	g_signal_new ("receive-caplimit",
		      REACTOR_TYPE_COM,
		      G_SIGNAL_RUN_LAST,
		      0, NULL, NULL, NULL,
		      G_TYPE_NONE, 1, G_TYPE_INT);

    /**
     * ReactorCom::receive-query:
     * @self: a #ReactorCom
     * @query: query flags
     *
     * Receive QUERY command.
     */
    signals[SIGNAL_RECEIVE_QUERY] =
	g_signal_new ("receive-query",
		      REACTOR_TYPE_COM,
		      G_SIGNAL_RUN_LAST,
		      0, NULL, NULL, NULL,
		      G_TYPE_NONE, 1, REACTOR_TYPE_COM_QUERY);

    /**
     * ReactorCom::receive-set:
     * @self: a #ReactorCom
     * @status: status flags to set
     *
     * Receive SET command.
     */
    signals[SIGNAL_RECEIVE_SET] =
	g_signal_new ("receive-set",
		      REACTOR_TYPE_COM,
		      G_SIGNAL_RUN_LAST,
		      0, NULL, NULL, NULL,
		      G_TYPE_NONE, 1, REACTOR_TYPE_COM_STATUS);

    /**
     * ReactorCom::receive-reset:
     * @self: a #ReactorCom
     * @status: status flags to reset
     *
     * Receive RESET command.
     */
    signals[SIGNAL_RECEIVE_RESET] =
	g_signal_new ("receive-reset",
		      REACTOR_TYPE_COM,
		      G_SIGNAL_RUN_LAST,
		      0, NULL, NULL, NULL,
		      G_TYPE_NONE, 1, REACTOR_TYPE_COM_STATUS);

    /**
     * ReactorCom::receive-bq769x0:
     * @self: a #ReactorCom
     * @action: a #ReactorComBq769x0
     * @address: register address
     * @size: number of bytes in @data
     * @data: (array length=size) (element-type guint8): register values
     *
     * Receive BQ769X0 command.
     */
    signals[SIGNAL_RECEIVE_BQ769X0] =
	g_signal_new ("receive-bq769x0",
		      REACTOR_TYPE_COM,
		      G_SIGNAL_RUN_LAST,
		      0, NULL, NULL, NULL,
		      G_TYPE_NONE, 4,
		      G_TYPE_INT, G_TYPE_INT,
		      G_TYPE_INT, G_TYPE_POINTER);

    props[PROP_RECEIVER] =
	g_param_spec_pointer ("receiver",
			      "receiver",
			      "internal reactor-com receiver instance",
			      G_PARAM_READABLE |
			      G_PARAM_STATIC_STRINGS);

    props[PROP_TRANSMITTER] =
	g_param_spec_pointer ("transmitter",
			      "transmitter",
			      "internal reactor-com transmitter instance",
			      G_PARAM_WRITABLE |
			      G_PARAM_STATIC_STRINGS);

    g_object_class_install_properties (gcls, N_PROPS, props);
}

static int8_t
api_ping (struct rcom *com,
	  uint8_t ack)
{
    g_signal_emit (((struct com_receiver *) com)->self,
		   signals[SIGNAL_RECEIVE_PING], 0,
		   (gint) ack);

    return 0;
}

static int8_t
api_timestamp (struct rcom *com,
	       int64_t timestamp)
{
    g_signal_emit (((struct com_receiver *) com)->self,
		   signals[SIGNAL_RECEIVE_TIMESTAMP], 0,
		   (gint64) timestamp);

    return 0;
}

static int8_t
api_message (struct rcom *com,
	     const char *message)
{
    g_signal_emit (((struct com_receiver *) com)->self,
		   signals[SIGNAL_RECEIVE_MESSAGE], 0,
		   (const gchar *) message);

    return 0;
}

static int8_t
api_voltage (struct rcom *com,
	     const int16_t *cells,
	     uint8_t count)
{
    g_signal_emit (((struct com_receiver *) com)->self,
		   signals[SIGNAL_RECEIVE_VOLTAGE], 0,
		   cells, (gint) count);

    return 0;
}

static int8_t
api_current (struct rcom *com,
	     int32_t current)
{
    g_message ("receive current");
    g_signal_emit (((struct com_receiver *) com)->self,
		   signals[SIGNAL_RECEIVE_CURRENT], 0, current);

    return 0;
}

static int8_t
api_status (struct rcom *com,
	    uint32_t status)
{
    g_signal_emit (((struct com_receiver *) com)->self,
		   signals[SIGNAL_RECEIVE_STATUS], 0, status);

     return 0;
}

static int8_t
api_protection (struct rcom *com,
		uint32_t protection)
{
    g_signal_emit (((struct com_receiver *) com)->self,
		   signals[SIGNAL_RECEIVE_PROTECTION], 0, protection);

     return 0;
}

static int8_t
api_balancing (struct rcom *com,
	       uint32_t balancing)
{
    g_signal_emit (((struct com_receiver *) com)->self,
		   signals[SIGNAL_RECEIVE_BALANCING], 0, balancing);

    return 0;
}

static int8_t
api_temperature (struct rcom *com,
		 const int16_t *temperatures,
		 uint8_t count)
{
    g_signal_emit (((struct com_receiver *) com)->self,
		   signals[SIGNAL_RECEIVE_TEMPERATURE], 0,
		   temperatures, (gint) count);

    return 0;
}

static int8_t
api_config (struct rcom *com,
	    const struct rcom_conf *config)
{
    g_autoptr (ReactorConfig) wrapper;

    wrapper = reactor_config_new_with_config (config);

    g_signal_emit (((struct com_receiver *) com)->self,
		   signals[SIGNAL_RECEIVE_CONFIG], 0,
		   wrapper);

    return 0;
}

static int8_t
api_caplimit (struct rcom *com,
	      uint8_t limit)
{
    g_signal_emit (((struct com_receiver *) com)->self,
		   signals[SIGNAL_RECEIVE_CAPLIMIT], 0, limit);

    return 0;
}

static int8_t
api_query (struct rcom *com,
	   uint32_t query)
{
    g_signal_emit (((struct com_receiver *) com)->self,
		   signals[SIGNAL_RECEIVE_QUERY], 0, query);

    return 0;
}

static int8_t
api_set (struct rcom *com,
	 uint32_t status)
{
    g_signal_emit (((struct com_receiver *) com)->self,
		   signals[SIGNAL_RECEIVE_SET], 0, status);

    return 0;
}

static int8_t
api_reset (struct rcom *com,
	   uint32_t status)
{
    g_signal_emit (((struct com_receiver *) com)->self,
		   signals[SIGNAL_RECEIVE_RESET], 0, status);

    return 0;
}

static int8_t
api_bq769x0 (struct rcom *com,
	     uint8_t action,
	     uint8_t address,
	     uint8_t size,
	     const uint8_t *data)
{
    g_signal_emit (((struct com_receiver *) com)->self,
		   signals[SIGNAL_RECEIVE_BQ769X0], 0,
		   (gint) action, (gint) address, (gint) size, data);

    return 0;
}

static void
finalize (GObject *gobj)
{
    G_OBJECT_CLASS (reactor_com_parent_class)->finalize (gobj);
}

static void
set_property (GObject *gobj,
	      guint prop_id,
	      const GValue *value,
	      GParamSpec *spec)
{
    ReactorCom *self;
    ReactorComPrivate *p;

    self = REACTOR_COM (gobj);
    p = reactor_com_get_instance_private (self);

    switch (prop_id) {
    case PROP_TRANSMITTER:
	g_assert_null (p->transmitter);
	p->transmitter = g_value_get_pointer (value);
	break;

    default:
	G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, prop_id, spec);
    }
}

static void
get_property (GObject *gobj,
	      guint prop_id,
	      GValue *value,
	      GParamSpec *spec)
{
    ReactorCom *self;
    ReactorComPrivate *p;

    self = REACTOR_COM (gobj);
    p = reactor_com_get_instance_private (self);

    switch (prop_id) {
    case PROP_RECEIVER:
	g_value_set_pointer (value, &p->receiver);
	break;

    default:
	G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, prop_id, spec);
    }
}

static struct rcom *
get_transmitter (ReactorCom *self)
{
    ReactorComPrivate *p;

    p = reactor_com_get_instance_private (self);
    g_assert_nonnull (p->transmitter);

    return p->transmitter;
}

#define RCALL_STRINGIFY(x) #x
#define RCALL(self, method, error, ...) do { \
	gint result; \
	result = rcom_##method (get_transmitter (self), ##__VA_ARGS__); \
	if (result < 0) { \
	    g_set_error (error, \
			REACTOR_COM_ERROR, \
			REACTOR_COM_ERROR_SEND, \
			"Failed to send %s", \
			RCALL_STRINGIFY (method)); \
	} \
	return result; \
    } while (0)

/**
 * reactor_com_send_ping:
 * @self: self
 * @ack: acknowledge argument
 * @error: (nullable): location of GError
 *
 * Pings communication. @acknowledge number tells if the receiver should
 * ping back. If value is greater than 0, receiver should ping back with
 * value lower by one. Higher ping value may be used to test communication
 * speed.
 *
 * returns: 0 on success, negative value on error
 */
gint8
reactor_com_send_ping (ReactorCom *self,
		       guint8 ack,
		       GError **error)
{
    RCALL (self, ping, error, ack);
}

/**
 * reactor_com_send_timestamp:
 * @self: self
 * @timestamp: current time in number of seconds since 1st Jan 1970
 * @error: (nullable): location of GError
 *
 * Sends current time.
 *
 * returns: 0 on success, negative value on error
 */
gint8
reactor_com_send_timestamp (ReactorCom *self,
			    gint64 timestamp,
			    GError **error)
{
    RCALL (self, timestamp, error, timestamp);
}

/**
 * reactor_com_send_message:
 * @self: self
 * @message: text message to send
 * @error: (nullable): location of GError
 *
 * Sends text message.
 *
 * returns: 0 on success, negative value on error
 */
gint8
reactor_com_send_message (ReactorCom *self,
			  const gchar *message,
			  GError **error)
{
    RCALL (self, message, error, message);
}

/**
 * reactor_com_send_voltage:
 * @self: self
 * @cells: (array length=count): cell voltages in millivolts
 * @count: number of cells
 * @error: (nullable): location of GError
 *
 * Sends cell count and their voltages.
 *
 * returns: 0 on success, negative value on error
 */
gint8
reactor_com_send_voltage (ReactorCom *self,
			  const gint16 *cells,
			  guint8 count,
			  GError **error)
{
    RCALL (self, voltage, error, cells, count);
}

/**
 * reactor_com_send_current:
 * @self: self
 * @current: battery current in miliampers
 * @error: (nullable): location of GError
 *
 * Sends battery current as positive value when charging or negative value when
 * discharging
 *
 * returns: 0 on success, negative value on error
 */
gint8
reactor_com_send_current (ReactorCom *self,
			  gint32 current,
			  GError **error)
{
    RCALL (self, current, error, current);
}

/**
 * reactor_com_send_status:
 * @self: self
 * @status: status flags
 * @error: (nullable): location of GError
 *
 * Sends status flags as bit values of @status argument. These values are
 * defined as RCOM_STATUS_* macros.
 *
 * returns: 0 on success, negative value on error
 */
gint8
reactor_com_send_status (ReactorCom *self,
			 ReactorComStatus status,
			 GError **error)
{
    RCALL (self, status, error, status);
}

/**
 * reactor_com_send_protection:
 * @self: self
 * @protection: protection flags
 * @error: (nullable): location of GError
 *
 * Sends protection flags as bit values of @protection argument. These values
 * are defines as RCOM_PROTECTION_* macros.
 *
 * returns: 0 on success, negative value on error
 */
gint8
reactor_com_send_protection (ReactorCom *self,
			     ReactorComProtection protection,
			     GError **error)
{
    RCALL (self, protection, error, protection);
}

/**
 * reactor_com_send_balancing:
 * @self: self
 * @balancing: balancing flags
 * @error: (nullable): location of GError
 *
 * Sends balancing flags. Each cell is represented as one bit in @balancing
 * argument.
 *
 * returns: 0 on success, negative value on error
 */
gint8
reactor_com_send_balancing (ReactorCom *self,
			    guint32 balancing,
			    GError **error)
{
    RCALL (self, balancing, error, balancing);
}

/**
 * reactor_com_send_temperature:
 * @self: self
 * @temperatures: (array length=count): temperature values
 * @count: number of temperature sensors
 * @error: (nullable): location of GError
 *
 * Sends temperature values in kelwin degrees.
 *
 * returns: 0 on success, negative value on error
 */
gint8
reactor_com_send_temperature (ReactorCom *self,
			      const gint16 *temperatures,
			      guint8 count,
			      GError **error)
{
    RCALL (self, temperature, error, temperatures, count);
}

/**
 * reactor_com_send_config:
 * @self: self
 * @config: ReactorConfig instance
 * @error: (nullable): location of GError
 *
 * Sends configuration.
 *
 * returns: 0 on success, negative value on error
 */
gint8
reactor_com_send_config (ReactorCom *self,
			 ReactorConfig *config,
			 GError **error)
{
    RCALL (self, config, error, reactor_config_get_config (config));
}

/**
 * reactor_com_send_caplimit:
 * @self: self
 * @limit: Battery percentage limit
 * @error: (nullable): location of GError
 *
 * Sends battery limit selection
 *
 * returns: 0 on success, negative value on error
 */
gint8
reactor_com_send_caplimit (ReactorCom *self,
			   guint8 limit,
			   GError **error)
{
    RCALL (self, caplimit, error, limit);
}

/**
 * reactor_com_send_query:
 * @self: self
 * @query: query flags
 * @error: (nullable): location of GError
 *
 * Sends query flags
 *
 * returns: 0 on success, negative value on error
 */
gint8
reactor_com_send_query (ReactorCom *self,
			ReactorComQuery query,
			GError **error)
{
    RCALL (self, query, error, query);
}

/**
 * reactor_com_send_set:
 * @self: a #ReactorCom
 * @status: status flags to set
 * @error: (nullable): location of GError
 *
 * Sends set command
 *
 * returns: 0 on success, negative value on error
 */
gint8
reactor_com_send_set (ReactorCom *self,
		      ReactorComStatus status,
		      GError **error)
{
    RCALL (self, set, error, status);
}

/**
 * reactor_com_send_reset:
 * @self: a #ReactorCom
 * @status: status flags to set
 * @error: (nullable): location of GError
 *
 * Sends reset command
 *
 * returns: 0 on success, negative value on error
 */
gint8
reactor_com_send_reset (ReactorCom *self,
			ReactorComStatus status,
			GError **error)
{
    RCALL (self, reset, error, status);
}

/**
 * reactor_com_send_bq769x0:
 * @self: a #ReactorCom
 * @action: action to do, a #ReactorComBq769x0 value
 * @address: register address
 * @size: size of @data
 * @data: (array length=size): written or read data
 * @error: (nullable): location of GError
 *
 * Sends bq769x0 command
 *
 * returns: 0 on success, negative value on error
 */
gint8
reactor_com_send_bq769x0 (ReactorCom *self,
			  ReactorComBq769x0 action,
			  guint8 address,
			  gint size,
			  const guint8 *data,
			  GError **error)
{
    RCALL (self, bq769x0, error, action, address, size, data);
}
